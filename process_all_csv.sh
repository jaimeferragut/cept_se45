# 30.07.2018 Jaime Ferragut [jaime.ferragut@ec.europa.eu]
# This bash script processes a group of .csv packet captures and prints the results into a .csv file
# Script requirements:
#   1) All .csv packet captures must be located in csv_files/
#   2) Each .csv capture must be accompanied by a .json file in config_files/ to describe the experiment
#   3) For each pair (<capture_name>.csv, <config_name>.json), <capture_name> = <config_name>

#!/bin/bash

# Hello
echo "Processing all experiments in csv_files/, please wait..."
echo

# Process all capture files in csv_files/
for full_filename in csv_files/*.csv; do
    # Call <process_single_csv.py> passing the packet capture filename as the only parameter
    python process_single_csv.py "$full_filename"
done

# Goodbye
echo "All experiments in csv_files/ have been processed, please find individual results in results/"