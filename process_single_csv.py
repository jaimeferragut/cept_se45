# This script processes a packet capture from an RLAN 6 GHz experiment and prints out various traffic statistics
# 26.07.2018 Jaime Ferragut <jaime.ferragut@ec.europa.eu>
# NOTE: the packet capture must be a .csv file exported from a .pcap/.wcap trace (e.g., with Wireshark)
# The .csv file must contain exactly the following columns displayed in exactly the following order:
#       +---------------------------------------------------------+
#       | Column | Field Description   | Wireshark field name     |
#       +---------------------------------------------------------+
#       |      0 | No.                 | frame.number             |
#       |      1 | Time                | frame.time_relative      |
#       |      2 | Source IP           | ip.src                   |
#       |      3 | Destination IP      | ip.dst                   |
#       |      4 | Type/Subtype        | wlan.fc.type_subtype     |
#       |      5 | Protocol            | [Custom field]           |
#       |      6 | Transmitter address | wlan.ta                  |
#       |      7 | Receiver address    | wlan.ra                  |
#       |      8 | Sequence number     | wlan.seq                 |
#       |      9 | Duration            | wlan_radio.duration      |
#       |     10 | Preamble            | wlan_radio.preamble      |
#       |     11 | Short GI            | wlan_radio.11ac.short_gi |
#       |     12 | MCS Index           | wlan_radio.11ac.mcs      |
#       |     13 | Data rate           | wlan_radio.data_rate     |
#       |     14 | FCS                 | wlan.fcs.status          |
#       |     15 | Length              | frame.len                |
#       |     16 | # Spatial streams   | radiotap.vht.nss.0       |
#       |     17 | Info                | [Custom field]           |
#       +---------------------------------------------------------+


# Function definitions
def is_dl_80211_ack(frame):
    """
    This function returns TRUE if <frame> is a DL 802.11 ACK, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is a DL 802.11 ACK according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x001D && wlan.ra==STA_MAC
    if frame[4]=="Acknowledgement" and frame[7]==STA_MAC:
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_ul_80211_ack(frame):
    """
    This function returns TRUE if <frame> is an UL 802.11 ACK, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is an UL 802.11 ACK according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x001D && wlan.ra==AP_MAC
    if frame[4]=="Acknowledgement" and frame[7]==AP_MAC:
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_dl_80211_block_ack(frame):
    """
    This function returns TRUE if <frame> is a DL 802.11 Block ACK, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is a DL 802.11 Block ACK according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x0019 && wlan.ta==AP_MAC && wlan.ra==STA_MAC
    if frame[4]=="802.11 Block Ack" and frame[6]==AP_MAC and frame[7]==STA_MAC:
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_ul_80211_block_ack(frame):
    """
    This function returns TRUE if <frame> is an UL 802.11 Block ACK, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is an UL 802.11 Block ACK according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x0019 && wlan.ta==STA_MAC && wlan.ra==AP_MAC
    if frame[4]=="802.11 Block Ack" and frame[6]==STA_MAC and frame[7]==AP_MAC:
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_rts_to_mac(frame, dst_mac):
    """
    This function returns TRUE if <frame> is an RTS control frame sent to <dst_mac>
    :param frame: frame from .csv trace
    :param dst_mac: RTS frame's destination MAC address
    :return: boolean
    """
    # Check if <frame> is an RTS frame sent to <dst_mac> according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x001b && wlan.ra==<dst_mac>
    if (frame[4] == "Request-to-send") and (frame[7] == dst_mac):
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_dl_cts(frame):
    """
    This function returns TRUE if <frame> is a DL Clear-to-Send (CTS), FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is a DL CTS according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x001C && wlan.ra==STA_MAC
    if frame[4]=="Clear-to-send" and frame[7]==STA_MAC:
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_ul_cts(frame):
    """
    This function returns TRUE if <frame> is an UL Clear-to-Send (CTS), FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is an UL CTS according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x001C && wlan.ra==AP_MAC
    if frame[4]=="Clear-to-send" and frame[7]==AP_MAC:
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_ap_beacon(frame):
    """
    This function returns TRUE if <frame> is an AP beacon, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is an AP beacon according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x0008 && wlan.ta==AP_MAC
    if frame[4]=="Beacon frame" and frame[6]==AP_MAC:
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_dummy_beacon(frame):
    """
    This function returns TRUE if <frame> is a beacon from the dummy AP, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is a beacon from the dummy AP according to the following Wireshark filter:
    # wlan.fc.type_subtype==0x0008 && wlan.ta==DUMMY_AP_MAC
    if frame[4] == "Beacon frame" and frame[6] == DUMMY_AP_MAC:
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_dl_data(frame):
    """
    This function returns TRUE if <frame> is a DL data frame, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is a downlink data frame according to the following Wireshark filter (as defined by HPE Aruba):
    # ip.src==SERVER_IP && ip.dst==STA_IP &&
    # wlan.ta==AP_MAC && wlan.ra==STA_MAC &&
    # wlan.fc.type_subtype==0x0028 && wlan.seq
    if (SERVER_IP in frame[2] and STA_IP in frame[3] and frame[6]==AP_MAC and
        frame[7]==STA_MAC and frame[4]=="QoS Data" and frame[8] != ''):
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_ul_data(frame):
    """
    This function returns TRUE if <frame> is an UL data frame, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is an uplink data frame according to the following Wireshark filter (as defined by HPE Aruba):
    # ip.src==STA_IP && ip.dst==SERVER_IP &&
    # wlan.ta==STA_MAC && wlan.ra==AP_MAC &&
    # wlan.fc.type_subtype==0x0028 && wlan.seq
    if (STA_IP in frame[2] and SERVER_IP in frame[3] and frame[6] == STA_MAC and
        frame[7] == AP_MAC and frame[4] == "QoS Data" and frame[8] != ''):
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_dl_tcp_retx(frame):
    """
    This function returns TRUE if <frame> contains a re-transmitted/out-of-order DL TCP segment, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is a re-transmitted/out-of-order DL TCP segment according to the following Wireshark filter:
    # ip.src==SERVER_IP && ip.dst==STA_IP && wlan.ta==AP_MAC && wlan.ra==STA_MAC &&
    # wlan.fc.type_subtype==0x0028 && wlan.seq &&
    # (tcp.analysis.retransmission || tcp.analysis.out_of_order)
    if is_dl_data(frame) and ("[TCP Retransmission]" in frame[17] or "[TCP Out-Of-Order]" in frame[17]):
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def is_ul_tcp_retx(frame):
    """
    This function returns TRUE if <frame> contains a re-transmitted/out-of-order UL TCP segment, FALSE otherwise
    :param frame: frame from .csv trace
    :return: boolean
    """
    # Check if <frame> is a re-transmitted/out-of-order UL TCP segment according to the following Wireshark filter:
    # ip.src==STA_IP && ip.dst==SERVER_IP && wlan.ta==STA_MAC && wlan.ra==AP_MAC &&
    # wlan.fc.type_subtype==0x0028 && wlan.seq &&
    # (tcp.analysis.retransmission || tcp.analysis.out_of_order)
    if is_ul_data(frame) and ("[TCP Retransmission]" in frame[17] or "[TCP Out-Of-Order]" in frame[17]):
        retval = True
    else:
        retval = False

    # Return boolean
    return retval


def convert_fields(csv_capture):
    """
    This function converts all frame fields in a .csv capture file into the corresponding data types (int, float)
    The following parameter mapping must be strictly followed in the .csv file:
    frame[0]="No."
    frame[1]="Time"
    frame[2]="Source IP"
    frame[3]="Destination IP"
    frame[4]="Type/Subtype"
    frame[5]="Protocol"
    frame[6]="Transmitter address"
    frame[7]="Receiver address"
    frame[8]="Sequence number"
    frame[9]="Duration"
    frame[10]="Preamble"
    frame[11]="Short GI"
    frame[12]="MCS"
    frame[13]="Data rate"
    frame[14]="FCS"
    frame[15]="Length"
    frame[16]="Spatial streams"
    frame[17]="Info"
    :param csv_capture: .csv capture
    :return: void
    """

    # Iterate over each frame in the .csv capture and convert selected fields into the appropriate data types
    for frame in csv_capture:

        frame[0] = int(frame[0])        # .pcap frame number
        frame[1] = float(frame[1])      # Arrival timestamp

        #TODO: understand why some 802.11 sequence numbers are blank
        try:
            frame[8] = int(frame[8])    # 802.11 frame sequence number (handle exception: some might be blank)
        except Exception:
            frame[8] = ''               # If empty, set to blank (cannot set to 0!)

        # TODO: understand why some frame durations are missing
        # Remove UTF-8 code for 'µ' character
        frame[9] = frame[9].replace('\\302\\265s', '')
        try:
            frame[9] = int(frame[9])    # 802.11 frame duration in us (handle exception: some might be blank)
        except Exception:
            frame[9] = 0                # If empty, set to 0

        # Remove UTF-8 code for 'µ' character
        frame[10] = frame[10].replace('\\302\\265s', '')
        try:
            frame[10] = int(frame[10])  # Preamble duration in us (handle exception: some might be blank)
        except Exception:
            frame[10] = 0               # If empty, set to 0

        try:
            frame[12] = int(frame[12])  # MCS index (handle exception: some might be blank)
        except Exception:
            frame[12] = -1              # If empty, set to -1 (cannot set to 0!)

        frame[13] = float(frame[13])    # Data rate (Mbps)
        frame[15] = int(frame[15])      # Length (bytes)

        try:
            frame[16] = int(frame[16])  # Number of MIMO spatial streams (handle exception: some might be blank)
        except Exception:
            frame[16] = ''              # If empty, set to blank (cannot set to 0!)


def categorise_traffic(csv_data, traffic_dict):
    """
    This function categorises each frame in <csv_data> into one of the keys in traffic_dict{}
    and returns a categorised traffic dictionary
    :param csv_data: .csv capture
    :param traffic_dict: traffic dictionary
    :return: traffic dictionary
    """
    # Categorise each frame from .csv capture and add it to traffic dictionary
    # Notice the power of combining the 'list' and 'filter' operators with lambda functions
    # DL data
    traffic_dict['dl_data'] = list(filter(is_dl_data, csv_data))
    traffic_dict['dl_data_no_retx'] = list(filter(lambda frame: is_dl_data(frame) and
                                                                not is_dl_tcp_retx(frame), csv_data))
    # UL data
    traffic_dict['ul_data'] = list(filter(is_ul_data, csv_data))
    traffic_dict['ul_data_no_retx'] = list(filter(lambda frame: is_ul_data(frame) and
                                                                not is_ul_tcp_retx(frame), csv_data))
    # TODO: do we really need this category?
    traffic_dict['dl_tcp_retx'] = list(filter(is_dl_tcp_retx, csv_data))
    traffic_dict['ul_tcp_retx'] = list(filter(is_ul_tcp_retx, csv_data))
    # 802.11 ACKs
    traffic_dict['dl_80211_ack'] = list(filter(is_dl_80211_ack, csv_data))
    traffic_dict['ul_80211_ack'] = list(filter(is_ul_80211_ack, csv_data))
    # 802.11 Block ACKs
    traffic_dict['dl_80211_block_ack'] = list(filter(is_dl_80211_block_ack, csv_data))
    traffic_dict['ul_80211_block_ack'] = list(filter(is_ul_80211_block_ack, csv_data))
    # RTS
    traffic_dict['dl_rts'] = list(filter(lambda frame: is_rts_to_mac(frame, STA_MAC), csv_data))
    traffic_dict['ul_rts'] = list(filter(lambda frame: is_rts_to_mac(frame, AP_MAC), csv_data))
    # CTS
    traffic_dict['dl_cts'] = list(filter(is_dl_cts, csv_data))
    traffic_dict['ul_cts'] = list(filter(is_ul_cts, csv_data))
    # AP beacons
    traffic_dict['ap_beacons'] = list(filter(is_ap_beacon, csv_data))
    # Dummy AP beacons
    traffic_dict['dummy_beacons'] = list(filter(is_dummy_beacon, csv_data))
    # Other frames
    traffic_dict['other'] = list(filter(lambda frame: (not is_dl_data(frame) and
                                                       not is_ul_data(frame) and
                                                       not is_dl_80211_ack(frame) and
                                                       not is_ul_80211_ack(frame) and
                                                       not is_dl_80211_block_ack(frame) and
                                                       not is_ul_80211_block_ack(frame) and
                                                       not is_rts_to_mac(frame, STA_MAC) and
                                                       not is_rts_to_mac(frame, AP_MAC) and
                                                       not is_dl_cts(frame) and
                                                       not is_ul_cts(frame) and
                                                       not is_ap_beacon(frame) and
                                                       not is_dummy_beacon(frame)), csv_data))

    # Return categorised traffic dictionary
    return traffic_dict


# TODO: abort function execution if wrong JSON file passed to script (e.g., 0 captured DL/UL frames)
def calculate_airtime_percentage(traffic_dict, airtime, airtime_pc_results):
    """
    This function calculates the percentage of airtime for each traffic category
    :param traffic_dict: traffic dictionary
    :param airtime: total airtime for frames in .csv capture
    :param airtime_pc_results: list to populate with traffic stats for .csv output
    :return: void
    """

    # Calculate airtime percentage for each traffic category
    # Data
    air_pc_dl_data = round(sum(item[9] for item in traffic_dict['dl_data'])/airtime*100, 2)
    air_pc_ul_data = round(sum(item[9] for item in traffic_dict['ul_data'])/airtime*100, 2)
    # 802.11 ACKs
    air_pc_dl_80211_ack = round(sum(item[9] for item in traffic_dict['dl_80211_ack'])/airtime*100, 2)
    air_pc_ul_80211_ack = round(sum(item[9] for item in traffic_dict['ul_80211_ack'])/airtime*100, 2)
    # 802.11 Block ACKs
    air_pc_dl_80211_block_ack = round(sum(item[9] for item in traffic_dict['dl_80211_block_ack'])/airtime*100, 2)
    air_pc_ul_80211_block_ack = round(sum(item[9] for item in traffic_dict['ul_80211_block_ack'])/airtime*100, 2)
    # RTS
    air_pc_dl_rts = round(sum(item[9] for item in traffic_dict['dl_rts'])/airtime*100, 2)
    air_pc_ul_rts = round(sum(item[9] for item in traffic_dict['ul_rts'])/airtime*100, 2)
    # CTS
    air_pc_dl_cts = round(sum(item[9] for item in traffic_dict['dl_cts'])/airtime*100, 2)
    air_pc_ul_cts = round(sum(item[9] for item in traffic_dict['ul_cts'])/airtime*100, 2)
    # Beacons
    air_pc_ap_beacons = round(sum(item[9] for item in traffic_dict['ap_beacons'])/airtime*100, 2)
    air_pc_dummy_beacons = round(sum(item[9] for item in traffic_dict['dummy_beacons'])/airtime*100, 2)
    # Other
    air_pc_other = round(sum(item[9] for item in traffic_dict['other'])/airtime*100, 2)

    # Add traffic stats to <airtime_pc_results>
    #  airtime_pc_results[0]:   %airtime(DL_data)
    #                    [1]:   %airtime(UL_data)
    #                    [2]:   %airtime(DL_ACK)
    #                    [3]:   %airtime(UL_ACK)
    #                    [4]:   %airtime(DL_BA)
    #                    [5]:   %airtime(UL_BA)
    #                    [6]:   %airtime(DL_RTS)
    #                    [7]:   %airtime(UL_RTS)
    #                    [8]:   %airtime(DL_CTS)
    #                    [9]:   %airtime(UL_CTS)
    #                    [10]:  %airtime(beacons)
    #                    [11]:  %airtime(dummy beacons)
    #                    [12]:  %airtime(other)

    # Data
    airtime_pc_results.append(air_pc_dl_data)
    airtime_pc_results.append(air_pc_ul_data)
    # 802.11 ACKs
    airtime_pc_results.append(air_pc_dl_80211_ack)
    airtime_pc_results.append(air_pc_ul_80211_ack)
    # 802.11 Block ACKs
    airtime_pc_results.append(air_pc_dl_80211_block_ack)
    airtime_pc_results.append(air_pc_ul_80211_block_ack)
    # RTS
    airtime_pc_results.append(air_pc_dl_rts)
    airtime_pc_results.append(air_pc_ul_rts)
    # CTS
    airtime_pc_results.append(air_pc_dl_cts)
    airtime_pc_results.append(air_pc_ul_cts)
    # Beacons
    airtime_pc_results.append(air_pc_ap_beacons)
    airtime_pc_results.append(air_pc_dummy_beacons)
    # Other
    airtime_pc_results.append(air_pc_other)


def calculate_missed_frames(dict, delta_threshold, traffic_category):
    """
    This function returns the number of missed frames in a specific traffic category
    This procedure is defined in HPE Aruba's Test Plan document
    :param dict: traffic dictionary
    :param delta_threshold: threshold used to exclude sn_delta outliers
    :param traffic_category: traffic category (key) in <dict>
    :return: number of missed frames in <traffic_category>
    """
    # Local variables
    sn_deltas = []
    retval = 0

    # Generate a list of sequence number deltas, s.t.:
    #   sn_delta = seq_num(frame(N+1)) - seq_num(frame(N))
    for (first, second) in zip(dict[traffic_category], dict[traffic_category][1:]):
        sn_deltas.append(second[8] - first[8])

    # Each sn_delta value has a different meaning:
    #   sn_delta = -4095 ------> No missed frames (sequence number has wrapped back to 0 after reaching 4095)
    #   -4095 < sn_delta < 1 --> Retransmitted frame
    #   sn_delta = 1 ----------> Consecutive frame
    #   sn_delta > 1 ----------> There is a gap of (sn_delta - 1) missed frames
    # TODO: HPE's method assumes blocks of retx frames with monotonically-increasing sequence numbers

    # Count the total number of missed frames in traffic category
    for item in sn_deltas:
        # Exclude sn_delta outliers from missed frames calculation
        if (1 < item < delta_threshold): retval += (item - 1)

    # Return number of missed frames
    return retval




# Main section

# Import modules
import glob, os, csv, sys, json, ntpath
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# Check number of input arguments
if len(sys.argv) != 2:
    print("[ERROR]: Wrong number of input arguments")
    print("[USAGE]: python process_single_csv.py <path_to_csv_capture_file>")
    exit(-1)

# Check if input .csv file exists in current directory
os.chdir(".")
if sys.argv[1] not in glob.glob(sys.argv[1]):
    print("[ERROR]: " + ntpath.basename(sys.argv[1]) + " capture file not found, please check csv_files/ and try again")
    exit(-1)
else:
    print("[OK]: " + ntpath.basename(sys.argv[1]) + " capture file found")

# Build path to experiment configuration file
filename = os.path.basename(sys.argv[1])                    # Read full pathname from input .csv file
(filename, ext) = os.path.splitext(filename)                # Split pathname into (filename, extension)
config_filename = "config_files/" + filename + ".json"      # Add config folder and .json extension to filename

# Open experiment configuration file
try:
    with open(config_filename) as json_data_file:
        config = json.load(json_data_file)
        print("[OK]: " + ntpath.basename(config_filename) + " configuration file found")

except Exception:
    print("[ERROR]: " + ntpath.basename(config_filename) +
          " configuration file not found, please check config_files/ and try again")
    exit(-1)

# Open devices list
try:
    with open('devices/device_list.json') as json_data_file:
        devices = json.load(json_data_file)
        print("[OK]: device list found")

except Exception:
    print("[ERROR]: device list not found, please check devices/ and try again")
    exit(-1)

# Read experiment configuration from JSON file
SERVER_IP = config['ip_addresses']['server']
STA_IP = config['ip_addresses']['sta']
STA_MAC = config['mac_addresses']['sta']
AP_MAC = config['mac_addresses']['ap']
DUMMY_AP_MAC = config['mac_addresses']['dummy_ap']

# Convert MAC addresses into symbolic names (using devices.json)
AP_NAME = devices['ap'][AP_MAC]
DUMMY_AP_NAME = devices['ap'][DUMMY_AP_MAC]
STA_NAME = devices['sta'][STA_MAC]

# Local variables
SN_DELTA_THRESHOLD = 100            # Threshold to exclude SN_deltas from Pc_DL, Pc_UL calculations
results = []                        # Empty list to write all experiment results to .csv file
airtime_pc_results = []             # Empty list to write all airtime percentage results for each traffic category
dl_data_frames = []                 # List of all downlink data frames (including TCP retx/out-of-order segments)
dl_data_no_retx_frames = []         # List of downlink data frames (excluding TCP retx/out-of-order segments)
ul_data_frames = []                 # List of all uplink data frames (including TCP retx/out-of-order segments)
ul_data_no_retx_frames = []         # List of uplink data frames (excluding TCP retx/out-of-order segments)
dl_tcp_retx_frames = []             # List of DL frames containing a re-transmitted/out-of-order TCP segment
ul_tcp_retx_frames = []             # List of UL frames containing a re-transmitted/out-of-order TCP segment
dl_80211_ack_frames = []            # List of downlink 802.11 ACK frames
ul_80211_ack_frames = []            # List of uplink 802.11 ACK frames
dl_80211_block_ack_frames = []      # List of downlink 802.11 Block ACK frames
ul_80211_block_ack_frames = []      # List of uplink 802.11 Block ACK frames
dl_rts_frames = []                  # List of DL RTS frames
ul_rts_frames = []                  # List of UL RTS frames
dl_cts_frames = []                  # List of DL CTS frames
ul_cts_frames = []                  # List of UL CTS frames
ap_beacons_frames = []              # List of AP beacons
dummy_beacons_frames = []           # List of dummy AP beacons
other_frames = []                   # List of 'other frames' (= none of the above)
categorised_frames = {              # Python dictionary to store categorised frames (see traffic categories below)
    'dl_data' : dl_data_frames,
    'dl_data_no_retx' : dl_data_no_retx_frames,
    'ul_data': ul_data_frames,
    'ul_data_no_retx': ul_data_no_retx_frames,
    'dl_tcp_retx': dl_tcp_retx_frames,
    'ul_tcp_retx': ul_tcp_retx_frames,
    'dl_80211_ack': dl_80211_ack_frames,
    'ul_80211_ack': ul_80211_ack_frames,
    'dl_80211_block_ack': dl_80211_block_ack_frames,
    'ul_80211_block_ack': ul_80211_block_ack_frames,
    'dl_rts': dl_rts_frames,
    'ul_rts': ul_rts_frames,
    'dl_cts': dl_cts_frames,
    'ul_cts': ul_cts_frames,
    'ap_beacons': ap_beacons_frames,
    'dummy_beacons': dummy_beacons_frames,
    'other': other_frames
}

# Read .csv file path
csv_file_path = sys.argv[1]

# Read entire .csv capture file into <data>
with open(csv_file_path) as csvfile:
    # Initialise iterator over .csv file
    reader = csv.reader(csvfile)
    # Skip first line (.csv legend)
    next(reader)
    # Write each row into <data>
    data = [row for row in reader]

# Convert selected fields in <data> into appropriate data types and handle potential exceptions
convert_fields(data)

# Categorise captured frames
categorised_frames = categorise_traffic(data, categorised_frames)

# Calculate total number of frames in .csv file
total_number_of_frames = len(data)

# Calculate MAC duty cycle or 'airtime utilisation' (Dc_MAC)
# HPE defines Dc_MAC as the sum of the duration of *all* captured frames (no exceptions!) divided by total capture time
#
# Formally:
#   Dc_MAC =  duration(all_frames) / total_capture_time

# Calculate total capture time
total_capture_time = data[-1][1] - data[0][1]

# Sum airtime for _all_ captured frames
airtime = sum(item[9] for item in data)

# Convert airtime to seconds and calculate MAC duty cycle
dc_mac = round(((airtime/1E6)/total_capture_time)*100, 2)

# Evaluate the quality of the packet capture for both downlink (Pc_DL) and uplink (Pc_UL)
# As proposed by HPE Aruba, a good estimate of the capture quality is the percentage of captured frames (Pc)
# HPE defines the percentage of captured frames as:
#   Pc = Nc / (Nc + Nm)
# Where:
#   Nc: number of captured non-retransmitted data frames
#   Nm: number of missed non-retransmitted data frames

# Calculate number of non-retransmitted {DL, UL} data frames
nc_dl = len(categorised_frames['dl_data_no_retx'])
nc_ul = len(categorised_frames['ul_data_no_retx'])

# TODO: Throw a warning if large {nm_dl, nm_ul} values found (e.g., ≥ 2000)
# Calculate number of missed non-retransmitted {DL, UL} frames
nm_dl = calculate_missed_frames(categorised_frames, SN_DELTA_THRESHOLD, 'dl_data_no_retx')
nm_ul = calculate_missed_frames(categorised_frames, SN_DELTA_THRESHOLD, 'ul_data_no_retx')

# Calculate Pc_DL, Pc_UL
pc_dl = round(nc_dl/(nc_dl+nm_dl)*100, 2)
pc_ul = round(nc_ul/(nc_ul+nm_ul)*100, 2)

# Print capture quality stats
print("Processing " + ntpath.basename(csv_file_path))
print("============================CAPTURE QUALITY===========================")
print("Percentage of captured DL data frames, excluding retx (Pc_DL):  %5.2f%%" % pc_dl)
print("Percentage of captured UL data frames, excluding retx (Pc_UL):  %5.2f%%" % pc_ul)

# Print stats for each traffic category
calculate_airtime_percentage(categorised_frames, airtime, airtime_pc_results)

# Print Dc_MAC results
print("============================MAC DUTY CYCLE============================")
print("MAC Duty cycle (Dc_MAC):                                         %3.2f%%" % dc_mac)
print("======================================================================")


# Write results onto local filesystem
global_results_dir = './results'
try:
    if not os.path.exists(global_results_dir):
        os.makedirs(global_results_dir)
except Exception:
    print('[ERROR]: Could not create results folder ' + global_results_dir)

# Create subdir to store current experiment results
filename = os.path.basename(sys.argv[1])                # Read full pathname from input .csv file
(filename, ext) = os.path.splitext(filename)            # Split pathname into (filename, extension)
results_subdir = global_results_dir + '/' + filename    # Output directory named after input .csv file

try:
    if not os.path.exists(results_subdir):
        os.makedirs(results_subdir)
except Exception:
        print ('[ERROR]: Could not create experiment folder ' + results_subdir)


# Plot airtime distribution by packet type
# Data
dl_data = [airtime_pc_results[0], airtime_pc_results[2], airtime_pc_results[4], airtime_pc_results[6],
           airtime_pc_results[8], airtime_pc_results[10], airtime_pc_results[11], 0]
ul_data = [airtime_pc_results[1], airtime_pc_results[3], airtime_pc_results[5], airtime_pc_results[7],
           airtime_pc_results[9], 0, 0, 0]
other_data = [0, 0, 0, 0, 0, 0, 0, airtime_pc_results[12]]

# Packet types
packet_types = ('Data', 'ACKs', 'BAs', 'RTS', 'CTS', 'AP beacons', 'Dummy beacons', 'Other')

# Set plot size
fig, ax = plt.subplots(figsize=(12, 4))

# Determine location and labels of X-ticks
x_pos = np.arange(len(packet_types))
plt.xticks(x_pos, packet_types)

# Set bar width
bar_width = 0.4

# Set plot/axes titles and layout
ax.set_title('Airtime distribution by packet type', fontweight='bold', fontsize=12)
ax.set_ylabel('% Airtime')
ax.set_axisbelow(True)
ax.grid(True, color='lightgray', linestyle='dashed')
fig.tight_layout()

# Plot bars (DL, UL, Other)
dl_bar = ax.bar(x_pos-bar_width/2, dl_data, bar_width, label="Downlink")
ul_bar = ax.bar(x_pos+bar_width/2, ul_data, bar_width, label="Uplink")
other_bar = ax.bar(x_pos, other_data, bar_width, label="Other")

# Show legend
plt.legend(loc="upper right")

# Save plot to results subdir
output_file = results_subdir + '/' + filename + '_airtime_distribution_by_packet_type.png'
plt.savefig(output_file, bbox_inches="tight", dpi=300)


# Calculate DL data bytes by 802.11ac data rate
# Initialise list
dl_data_bytes_by_rate = []

# For each DL data frame read ('Data rate', 'Frame length') and append to list
for frame in categorised_frames['dl_data']:
    dl_data_bytes_by_rate.append(((int(round(frame[13],0))), frame[15]))

# Define labels for pandas dataframe
labels = ['Data rate', 'Bytes']

# Convert list to pandas dataframe
df_dl_bytes_by_rate = pd.DataFrame.from_records(dl_data_bytes_by_rate, columns=labels)

# Group dataframe by 'Data rate' and sum 'Bytes' column
df_dl_bytes_by_rate = df_dl_bytes_by_rate.groupby(['Data rate'], as_index=False).sum()

# Add percentage column to dataframe
df_dl_bytes_by_rate['%Bytes'] = round(df_dl_bytes_by_rate.Bytes/df_dl_bytes_by_rate.Bytes.sum()*100, 2)

# Sort dataframe in descending order for easier reading
df_dl_bytes_by_rate = df_dl_bytes_by_rate.sort_values(by='Data rate', ascending=0)

# Calculate average DL data rate
avg_dl_data_rate = round(np.average(df_dl_bytes_by_rate['Data rate'], weights=df_dl_bytes_by_rate['%Bytes']),2)

# For a nicer display, convert data rates in pandas dataframe to string
df_dl_bytes_by_rate['Data rate'] = df_dl_bytes_by_rate['Data rate'].astype(str)

# Write dataframe to .csv file
output_file = results_subdir + '/' + filename + '_DL_dataRate_distribution.csv'
df_dl_bytes_by_rate.to_csv(output_file)


# Calculate DL data bytes by MCS index
# Initialise list
dl_traffic_by_mcs = []

# For each DL data frame read ('MCS', 'Frame length') and append to list
# For a nicer display, convert MCS index to string
for frame in categorised_frames['dl_data']:
    dl_traffic_by_mcs.append((str(frame[12]),frame[15]))

# Define labels for pandas dataframe
labels = ['MCS', 'Bytes']

# Convert list to pandas dataframe
df_dl_bytes_by_mcs = pd.DataFrame.from_records(dl_traffic_by_mcs, columns=labels)

# Group dataframe by 'MCS' and sum 'Bytes' column
df_dl_bytes_by_mcs = df_dl_bytes_by_mcs.groupby(['MCS'], as_index=False).sum()

# Add percentage column to dataframe
df_dl_bytes_by_mcs['%Bytes'] = round(df_dl_bytes_by_mcs.Bytes/df_dl_bytes_by_mcs.Bytes.sum()*100, 2)

# Sort dataframe in descending order for easier reading
df_dl_bytes_by_mcs = df_dl_bytes_by_mcs.sort_values(by='MCS', ascending=0)

# Write pandas dataframe to .csv file
output_file = results_subdir + '/' + filename + '_DL_mcs_distribution.csv'
df_dl_bytes_by_mcs.to_csv(output_file)


# Calculate DL data bytes by number of spatial streams
# Initialise list
dl_traffic_by_ss = []

# For each DL data frame read ('Number of spatial streams', 'Frame length') and append to list
for frame in categorised_frames['dl_data']:
    dl_traffic_by_ss.append((frame[16], frame[15]))

# Define labels for pandas dataframe
labels = ['SS', 'Bytes']

# Convert list to pandas dataframe
df_dl_bytes_by_ss = pd.DataFrame.from_records(dl_traffic_by_ss, columns=labels)

# Group dataframe by 'SS' and sum 'Bytes' column
df_dl_bytes_by_ss = df_dl_bytes_by_ss.groupby(['SS'], as_index=False).sum()

# Add percentage column to dataframe
df_dl_bytes_by_ss['%Bytes'] = round(df_dl_bytes_by_ss.Bytes/df_dl_bytes_by_ss.Bytes.sum()*100, 2)

# Sort dataframe in descending order for easier reading
df_dl_bytes_by_ss = df_dl_bytes_by_ss.sort_values(by='SS', ascending=0)

# Write pandas dataframe to .csv file
output_file = results_subdir + '/' + filename + '_DL_SS_distribution.csv'
df_dl_bytes_by_ss.to_csv(output_file)


# Create multi-plot figure with DL data distribution by different parameters
# Set size for global figure (12"x18")
plt.figure(figsize=(12,18))

# Set colour palette ("Tableau 20" colors as RGB values)
tableau20 = [(31, 119, 180), (174, 199, 232), (255, 127, 14), (255, 187, 120),
             (44, 160, 44), (152, 223, 138), (214, 39, 40), (255, 152, 150),
             (148, 103, 189), (197, 176, 213), (140, 86, 75), (196, 156, 148),
             (227, 119, 194), (247, 182, 210), (127, 127, 127), (199, 199, 199),
             (188, 189, 34), (219, 219, 141), (23, 190, 207), (158, 218, 229)]

# Scale Tableau20's RGB values to the [0,1] range, which is the format matplotlib accepts
for i in range(len(tableau20)):
    r, g, b = tableau20[i]
    tableau20[i] = (r/255., g/255., b/255.)


# Subplot 1 of 3: DL bytes by 802.11ac data rate
plt.subplot(131)
plt.title('(a) DL bytes by 802.11ac data rate', fontweight='bold', fontsize=16)
plt.pie(
    # Use 'Bytes' as plot data
    df_dl_bytes_by_rate['Bytes'],
    # Don't show labels on pie chart
    labels=None,
    # Show percentage on each wedge
    autopct='%1.2f%%',
    # Show percentages in bold
    textprops=dict(weight="bold", fontsize=18),
    # Use tableau20's colour palette
    colors=tableau20
    )
# Draw pie chart as circle
plt.axis('equal')
# Add subplot legend (add 'Mbps' to each data rate value)
plt.legend(labels=df_dl_bytes_by_rate['Data rate'] + ' Mbps', bbox_to_anchor=(1,0), loc='upper right', fontsize=18)


# Subplot 2 of 3: DL bytes by MCS index
plt.subplot(132)
plt.title('(b) DL bytes by MCS index', fontweight='bold', fontsize=16)
plt.pie(
    # Use 'Bytes' as plot data
    df_dl_bytes_by_mcs['Bytes'],
    # Don't show labels on pie chart
    labels=None,
    # Show percentage on each wedge
    autopct='%1.2f%%',
    # Show percentages in bold
    textprops=dict(weight="bold", fontsize=18),
    # Use tableau20's colour palette
    colors=tableau20
    )
# Draw pie chart as circle
plt.axis('equal')
# Add subplot legend (add 'MCS=' to each MCS index value)
plt.legend(labels='MCS=' + df_dl_bytes_by_mcs['MCS'], bbox_to_anchor=(1,0), loc="upper right", fontsize=18)

# Subplot 3 of 3: Dl bytes by number of MIMO spatial streams
plt.subplot(133)
plt.title('(c) DL bytes by number of spatial streams', fontweight='bold', fontsize=16)
plt.pie(
    # Use 'Bytes' as data
    df_dl_bytes_by_ss['Bytes'],
    # Don't show labels on pie chart
    labels=None,
    # Show percentage on each wedge
    autopct='%1.2f%%',
    # Show percentages in bold
    textprops=dict(weight="bold", fontsize=18),
    # Use tableau20's colour palette
    colors=tableau20
    )
# Draw pie chart as circle
plt.axis('equal')
# Add subplot legend (add 'SS' to each spatial streams value)
plt.legend(labels=df_dl_bytes_by_ss['SS'].astype(str) + ' SS', bbox_to_anchor=(1,0), loc="upper right", fontsize=18)

# Adjust subplots layout within the global figure
plt.subplots_adjust(left=0, right=1.25, bottom=0, top=0.275)

# Save global figure to results subdir
output_file = results_subdir + '/' + filename + '_DL_data_distribution.png'
plt.savefig(output_file, bbox_inches="tight", dpi=300)


# Calculate DL data bytes by MCS and GI
dl_traffic_by_mcs_gi = []

# For each DL data frame read ('MCS', 'Short GI', 'Length') and append to list
for frame in categorised_frames['dl_data']:
    # Determine GI ('short' vs. 'long')
    if frame[11] == 'True':
        gi = 'S'
    else:
        gi = 'L'
    # Append values to list
    dl_traffic_by_mcs_gi.append((str(frame[12])+gi, frame[15]))

# Define labels for pandas dataframe
labels = ['MCS_GI', 'Bytes']

# Convert list to pandas dataframe
df_dl_bytes_by_mcs_gi = pd.DataFrame.from_records(dl_traffic_by_mcs_gi, columns=labels)

# Group dataframe by 'MCS' and sum 'Bytes' column
df_dl_bytes_by_mcs_gi = df_dl_bytes_by_mcs_gi.groupby(['MCS_GI'], as_index=False).sum()

# Add missing MCS to dataframe
bins = ['0L','0S','1L','1S','2L','2S','3L','3S','4L','4S','5L','5S','6L','6S','7L','7S','8L','8S','9L','9S']
for mcs_gi in bins:
    if mcs_gi not in list(df_dl_bytes_by_mcs_gi['MCS_GI']):
        d = {'MCS_GI': [mcs_gi], 'Bytes': [0]}
        new_df = pd.DataFrame(data=d)
        df_dl_bytes_by_mcs_gi = df_dl_bytes_by_mcs_gi.append(new_df, ignore_index=True)

# Add percentage column to dataframe
df_dl_bytes_by_mcs_gi['%Bytes'] = round(df_dl_bytes_by_mcs_gi.Bytes/df_dl_bytes_by_mcs_gi.Bytes.sum(), 2)

# Sort dataframe in descending order for easier reading
df_dl_bytes_by_mcs_gi = df_dl_bytes_by_mcs_gi.sort_values(by='MCS_GI', ascending=1)

# Re-index dataframe
df_dl_bytes_by_mcs_gi = df_dl_bytes_by_mcs_gi.reset_index(drop=True)

# Calculate cumulative sum of '%Bytes' column
df_dl_bytes_by_mcs_gi['%CumSum'] = df_dl_bytes_by_mcs_gi['%Bytes'].cumsum(axis = 0)

# Plot CDF of % DL data bytes by MCS index
# Set plot size
fig, ax = plt.subplots(figsize=(9, 4))

# Set plot properties
ax.step(bins, df_dl_bytes_by_mcs_gi['%CumSum'], where='post')
ax.set_axisbelow(True)
ax.grid(True, color='lightgray', linestyle='dashed')
ax.set_title('CDF of downlink data bytes by (MCS, GI)', fontweight='bold', fontsize=12)
ax.set_xlabel('(MCS, GI)')
ax.set_ylabel('Cumulative percent')
ax.annotate('Avg DL data rate='+str(avg_dl_data_rate)+' Mbps', xy=(0.05, 0.8),
            xycoords='axes fraction', fontweight='bold', fontsize=10)

# Save plot to results subdir
output_file = results_subdir + '/' + filename + '_CDF_DL_data_bytes_by_MCS_GI.png'
plt.savefig(output_file, bbox_inches="tight", dpi=300)

# Write pandas dataframe to .csv file
output_file = results_subdir + '/' + filename + '_DL_mcs_gi_CDF.csv'
df_dl_bytes_by_mcs_gi.to_csv(output_file)



# Calculate UL data bytes by 802.11ac data rate
# Initialise list
ul_data_bytes_by_rate = []

# For each UL data frame read ('Data rate', 'Frame length') and append to list
# For a nicer display, round data rate to nearest integer and convert to string
for frame in categorised_frames['ul_data']:
    ul_data_bytes_by_rate.append((int(round(frame[13], 0)), frame[15]))

# Define labels for pandas dataframe
labels = ['Data rate', 'Bytes']

# Convert list to pandas dataframe
df_ul_bytes_by_rate = pd.DataFrame.from_records(ul_data_bytes_by_rate, columns=labels)

# Group dataframe by 'Data rate' and sum 'Bytes' column
df_ul_bytes_by_rate = df_ul_bytes_by_rate.groupby(['Data rate'], as_index=False).sum()

# Add percentage column to dataframe
df_ul_bytes_by_rate['%Bytes'] = round(df_ul_bytes_by_rate.Bytes/df_ul_bytes_by_rate.Bytes.sum()*100, 2)

# Sort dataframe in descending order for easier reading
df_ul_bytes_by_rate = df_ul_bytes_by_rate.sort_values(by='Data rate', ascending=0)

# Calculate average DL data rate
avg_ul_data_rate = round(np.average(df_ul_bytes_by_rate['Data rate'], weights=df_ul_bytes_by_rate['%Bytes']),2)

# For a nicer display, convert data rates in pandas dataframe to string
df_ul_bytes_by_rate['Data rate'] = df_ul_bytes_by_rate['Data rate'].astype(str)

# Write dataframe to .csv file
output_file = results_subdir + '/' + filename + '_UL_dataRate_distribution.csv'
df_ul_bytes_by_rate.to_csv(output_file)


# Calculate UL data bytes by MCS index
# Initialise list
ul_traffic_by_mcs = []

# For each UL data frame read ('MCS', 'Frame length') and append to list
# For a nicer display, convert MCS index to string
for frame in categorised_frames['ul_data']:
    if (frame[12] != -1):
        # UL data frame with valid MCS ---> append to list
        ul_traffic_by_mcs.append((str(frame[12]),frame[15]))
    # else:
        # UL data frame with MCS == -1 ---> do not append to list

# Define labels for pandas dataframe
labels = ['MCS', 'Bytes']

# Convert list to pandas dataframe
df_ul_bytes_by_mcs = pd.DataFrame.from_records(ul_traffic_by_mcs, columns=labels)

# Group dataframe by 'MCS' and sum 'Bytes' column
df_ul_bytes_by_mcs = df_ul_bytes_by_mcs.groupby(['MCS'], as_index=False).sum()

# Add percentage column to dataframe
df_ul_bytes_by_mcs['%Bytes'] = round(df_ul_bytes_by_mcs.Bytes/df_ul_bytes_by_mcs.Bytes.sum()*100, 2)

# Sort dataframe in descending order for easier reading
df_ul_bytes_by_mcs = df_ul_bytes_by_mcs.sort_values(by='MCS', ascending=0)

# Write pandas dataframe to .csv file
output_file = results_subdir + '/' + filename + '_UL_mcs_distribution.csv'
df_ul_bytes_by_mcs.to_csv(output_file)


# Calculate UL data bytes by number of spatial streams
# Initialise list
ul_traffic_by_ss = []

# For each UL data frame read ('Number of spatial streams', 'Frame length') and append to list
for frame in categorised_frames['ul_data']:
    if (frame[16] != ''):
        # UL data frame with valid #SS ---> append to list
        ul_traffic_by_ss.append((frame[16], frame[15]))
    # else:
        # UL data frame with missing number of SS ---> do not append to list

# Define labels for pandas dataframe
labels = ['SS', 'Bytes']

# Convert list to pandas dataframe
df_ul_bytes_by_ss = pd.DataFrame.from_records(ul_traffic_by_ss, columns=labels)

# Group dataframe by 'SS' and sum 'Bytes' column
df_ul_bytes_by_ss = df_ul_bytes_by_ss.groupby(['SS'], as_index=False).sum()

# Add percentage column to dataframe
df_ul_bytes_by_ss['%Bytes'] = round(df_ul_bytes_by_ss.Bytes/df_ul_bytes_by_ss.Bytes.sum()*100, 2)

# Sort dataframe in descending order for easier reading
df_ul_bytes_by_ss = df_ul_bytes_by_ss.sort_values(by='SS', ascending=0)

# Write pandas dataframe to .csv file
output_file = results_subdir + '/' + filename + '_UL_SS_distribution.csv'
df_ul_bytes_by_ss.to_csv(output_file)


# Create multi-plot figure with UL data distribution by different parameters
# Set size for global figure (12"x18")
plt.figure(figsize=(12,18))


# Subplot 1 of 3: UL bytes by 802.11ac data rate
plt.subplot(131)
plt.title('(a) UL bytes by 802.11ac data rate', fontweight='bold', fontsize=16)
plt.pie(
    # Use 'Bytes' as plot data
    df_ul_bytes_by_rate['Bytes'],
    # Don't show labels on pie chart
    labels=None,
    # Show percentage on each wedge
    autopct='%1.2f%%',
    # Show percentages in bold
    textprops=dict(weight="bold", fontsize=18),
    # Use tableau20's colour palette
    colors=tableau20
    )
# Draw pie chart as circle
plt.axis('equal')
# Add subplot legend (add 'Mbps' to each data rate value)
plt.legend(labels=df_ul_bytes_by_rate['Data rate'] + ' Mbps', bbox_to_anchor=(1,0), loc='upper right', fontsize=18)


# Subplot 2 of 3: UL bytes by MCS index
plt.subplot(132)
plt.title('(b) UL bytes by MCS index', fontweight='bold', fontsize=16)
plt.pie(
    # Use 'Bytes' as plot data
    df_ul_bytes_by_mcs['Bytes'],
    # Don't show labels on pie chart
    labels=None,
    # Show percentage on each wedge
    autopct='%1.2f%%',
    # Show percentages in bold
    textprops=dict(weight="bold", fontsize=18),
    # Use tableau20's colour palette
    colors=tableau20
    )
# Draw pie chart as circle
plt.axis('equal')
# Add subplot legend (add 'MCS=' to each MCS index value)
plt.legend(labels='MCS=' + df_ul_bytes_by_mcs['MCS'], bbox_to_anchor=(1,0), loc="upper right", fontsize=18)


# Subplot 3 of 3: UL bytes by number of MIMO spatial streams
plt.subplot(133)
plt.title('(c) UL bytes by number of spatial streams', fontweight='bold', fontsize=16)
plt.pie(
    # Use 'Bytes' as data
    df_ul_bytes_by_ss['Bytes'],
    # Don't show labels on pie chart
    labels=None,
    # Show percentage on each wedge
    autopct='%1.2f%%',
    # Show percentages in bold
    textprops=dict(weight="bold", fontsize=18),
    # Use tableau20's colour palette
    colors=tableau20
    )
# Draw pie chart as circle
plt.axis('equal')
# Add subplot legend (add 'SS' to each spatial streams value)
plt.legend(labels=df_ul_bytes_by_ss['SS'].astype(str) + ' SS', bbox_to_anchor=(1,0), loc="upper right", fontsize=18)

# Adjust subplots layout within the global figure
plt.subplots_adjust(left=0, right=1.25, bottom=0, top=0.275)

# Save global figure to results subdir
output_file = results_subdir + '/' + filename + '_UL_data_distribution.png'
plt.savefig(output_file, bbox_inches="tight", dpi=300)


# Calculate UL data bytes by MCS and GI
ul_traffic_by_mcs_gi = []

# For each UL data frame read ('MCS', 'Short GI', 'Length') and append to list
for frame in categorised_frames['ul_data']:
    # Determine GI ('short' vs. 'long')
    if frame[11] == 'True':
        gi = 'S'
    else:
        gi = 'L'

    if frame[12] != -1:
        # UL data frame with valid MCS index ---> append to list
        ul_traffic_by_mcs_gi.append((str(frame[12]) + gi, frame[15]))

# Define labels for pandas dataframe
labels = ['MCS_GI', 'Bytes']

# Convert list to pandas dataframe
df_ul_bytes_by_mcs_gi = pd.DataFrame.from_records(ul_traffic_by_mcs_gi, columns=labels)

# Group dataframe by 'MCS' and sum 'Bytes' column
df_ul_bytes_by_mcs_gi = df_ul_bytes_by_mcs_gi.groupby(['MCS_GI'], as_index=False).sum()

# Add missing MCS to dataframe
for mcs_gi in bins:
    if mcs_gi not in list(df_ul_bytes_by_mcs_gi['MCS_GI']):
        d = {'MCS_GI': [mcs_gi], 'Bytes': [0]}
        new_df = pd.DataFrame(data=d)
        df_ul_bytes_by_mcs_gi = df_ul_bytes_by_mcs_gi.append(new_df, ignore_index=True)

# Add percentage column to dataframe
df_ul_bytes_by_mcs_gi['%Bytes'] = round(df_ul_bytes_by_mcs_gi.Bytes/df_ul_bytes_by_mcs_gi.Bytes.sum(), 2)

# Sort dataframe in descending order for easier reading
df_ul_bytes_by_mcs_gi = df_ul_bytes_by_mcs_gi.sort_values(by='MCS_GI', ascending=1)

# Re-index dataframe
df_ul_bytes_by_mcs_gi = df_ul_bytes_by_mcs_gi.reset_index(drop=True)

# Calculate cumulative sum of '%Bytes' column
df_ul_bytes_by_mcs_gi['%CumSum'] = df_ul_bytes_by_mcs_gi['%Bytes'].cumsum(axis = 0)

# Plot CDF of % UL data bytes by MCS index
# Set plot size
fig, ax = plt.subplots(figsize=(9, 4))

# Set plot properties
ax.step(bins, df_ul_bytes_by_mcs_gi['%CumSum'], where='post')
ax.set_axisbelow(True)
ax.grid(True, color='lightgray', linestyle='dashed')
ax.set_title('CDF of uplink data bytes by (MCS, GI)', fontweight='bold', fontsize=12)
ax.set_xlabel('(MCS, GI)')
ax.set_ylabel('Cumulative percent')
ax.annotate('Avg UL data rate=' + str(avg_ul_data_rate) + ' Mbps', xy=(0.05, 0.8),
            xycoords='axes fraction', fontweight='bold', fontsize=10)

# Save plot to results subdir
output_file = results_subdir + '/' + filename + '_CDF_UL_data_bytes_by_MCS_GI.png'
plt.savefig(output_file, bbox_inches="tight", dpi=300)

# Write pandas dataframe to .csv file
output_file = results_subdir + '/' + filename + '_UL_mcs_gi_CDF.csv'
df_ul_bytes_by_mcs_gi.to_csv(output_file)


# Calculate DL MCS+GI index vs time
# Initialise list
dl_mcs_vs_time = []

# For each DL data frame read ('Time', 'MCS', 'Short GI')
# For a more meaningful display, convert 'Short GI' boolean to string (S,L)
for frame in categorised_frames['dl_data']:
    if frame[11] == 'True':
        gi_label = 'S'              # Short GI
    else:
        gi_label = 'L'              # Long GI

    # Append ('Time', 'MCS+GI') to list
    dl_mcs_vs_time.append((frame[1], str(frame[12]) + gi_label))

# Define labels for pandas dataframe
labels = ['Time', 'MCS+GI']

# Convert list to pandas dataframe
df_dl_mcs_vs_time = pd.DataFrame.from_records(dl_mcs_vs_time, columns=labels)

# Sort Y-axis from highest to lowest MCS+GI
df_dl_mcs_vs_time.sort_values('MCS+GI', ascending=True, inplace=True)

# Set plot size
fig, ax = plt.subplots(figsize=(20, 4))

# Plot pandas dataframe
ax.scatter(df_dl_mcs_vs_time['Time'], df_dl_mcs_vs_time['MCS+GI'], marker='.', s=0.5)

# Add axes labels
plt.xlabel('Time (s)', fontweight='bold')
plt.ylabel('MCS index', fontweight='bold')

# Save plot to results subdir
output_file = results_subdir + '/' + filename + '_MCS+GI_vs_time.png'
plt.savefig(output_file, bbox_inches="tight", dpi=300)


# Calculate Dc_MAC vs time (moving average)
# Build a ('Time', 'Duration') time series with all captured frames. Convert .pcap timestamps into pandas datetimes
# frame[9]: duration (usec)
# frame[1]: .pcap timestamp
ts_duration = pd.Series(data=(frame[9] for frame in data),
                        index=(pd.to_datetime(frame[1], unit='s') for frame in data))

# Define window length (in seconds) to calculate moving average
win_length = 5
win_length_label = str(win_length) + 's'    # <win_length> seconds

# Calculate Dc_MAC's moving average
ts_dc_mac = ts_duration.rolling(window=win_length_label).sum()/(win_length*1E6)*100

# Set plot size
fig, ax = plt.subplots(figsize=(15, 4))

# Convert time series index into elapsed seconds
ts_dc_mac.index = (ts_dc_mac.index - ts_dc_mac.index[0]).total_seconds()

# Plot time series
ts_dc_mac.plot(linewidth=0.75)

# Add horizontal dotted line for y=Dc_MAC
ax.axhline(dc_mac, color='red', linestyle='-.', linewidth=0.85)

# Add plot grid and axes labels
ax.grid(True, color='lightgray', linestyle='dashed')
plt.xlabel('Time (s)', fontweight='bold')
plt.ylabel('Dc_MAC (%)', fontweight='bold')

# Save plot to results subdir
output_file = results_subdir + '/' + filename + '_Dc_MAC_vs_time.png'
plt.savefig(output_file, bbox_inches="tight", dpi=300)



# Build results file
# Extract filename from <csv_file_path> and add it to <results>
results.append(ntpath.basename(csv_file_path))

# Add capture duration to <results>
results.append(total_capture_time)

# Add capture quality stats to <results>
results.append(pc_dl)
results.append(pc_ul)

# Add {AP, STA, Dummy AP} MAC addresses to <results>
results.append(AP_NAME)
results.append(STA_NAME)
results.append(DUMMY_AP_NAME)

# Add average {DL, UL} data rate to <results>
results.append(avg_dl_data_rate)
results.append(avg_ul_data_rate)

# Add max(#DL_SS) and %DL bytes @ max(#DL_SS) to <results>
index = df_dl_bytes_by_ss['SS'].idxmax()
results.append(df_dl_bytes_by_ss['SS'][index])
results.append(df_dl_bytes_by_ss['%Bytes'][index])

# Add RTS-CTS airtime % to <results>
results.append(round(airtime_pc_results[6] +
                     airtime_pc_results[7] +
                     airtime_pc_results[8] +
                     airtime_pc_results[9], 2))

# Add Dc_MAC stats to <results>
results.append(dc_mac)

# Create output .csv file with summary of results
# Initialise legend for output .csv file
csv_legend = ['Filename',
              'Duration',
              'Pc_DL',
              'Pc_UL',
              'AP',
              'STA',
              'Dummy AP',
              'Avg DL data rate',
              'Avg UL data rate',
              'Max(#DL_SS)', '%DL bytes',
              '%Air(RTS-CTS)',
              'Dc_MAC']

# Open output .csv file with summary of results (create file if it does not exist)
filename = global_results_dir + '/' + '0_Summary_of_results.csv'
output_file = open(filename, 'a+')

# Initialise csv writer on output file
wr = csv.writer(output_file)

# Check if results .csv file is empty
if os.stat(filename).st_size == 0:
    # Empty file, add csv legend as first row
    wr.writerow(csv_legend)

# Append results to output csv file
wr.writerow(results)

# Goodbye
print(ntpath.basename(csv_file_path) + " processed successfully, please find experiment results in results/\n")