@ECHO off

REM **************************************************************************
REM * This batch script streams <video_file> using HTTP over TCP port <port> *
REM * USAGE: vlc_server.bat <video_file> <TCP_port>                          *
REM * NOTE:  <video_file> MUST be located in C:\Users\RSL\Videos\RLAN6\      *
REM **************************************************************************
REM * Jaime Ferragut, 25.06.2018                                             *
REM **************************************************************************

REM Check input parameters
IF "%1"=="" GOTO ERROR
IF "%2"=="" GOTO ERROR

:PARAMS_OK
SET app="C:\Program Files (x86)\VideoLAN\VLC\vlc.exe"
SET video_file=C:\Users\RSL\Videos\RLAN6\%1
SET vlc_options=:sout=#transcode:duplicate{dst=gather:http{mux=ffmpeg{mux=flv},dst=:%2/},dst=display,ttl=5} :sout-all :sout-keep

REM Launch VLC and start HTTP streaming
CALL %app% %video_file% %vlc_options%
EXIT /B

:ERROR
ECHO USAGE: vlc_server.bat ^<video_file^> ^<tcp_port^>